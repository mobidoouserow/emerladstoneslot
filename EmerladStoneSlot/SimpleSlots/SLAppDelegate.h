//
//  SLAppDelegate.h
//  Slots
//
//  Created by Mup Userow on 28/04/2016.
//  Copyright (c) 2016 MD.io. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtilities.h"

@class ViewController;
//@class SLMainViewController;
@interface SLAppDelegate : NSObject <UIApplicationDelegate> {
    BOOL showAds;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
//@property (strong, nonatomic) SLMainViewController *mainViewController;
@property (strong, nonatomic) UIViewController *webviewController;
@property (strong, nonatomic) UIWebView *webView;


-(void)closeBackground;
;

@end
