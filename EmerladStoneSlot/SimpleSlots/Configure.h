// -----------------------------------
// GAME SETTINGS
// -----------------------------------
#define INITIAL_STARTUP_COINS @"300"
#define INITIAL_STARTUP_BET @"200"
#define INITIAL_LINES_COUNT @"20"
#define LABEL_WELCOME @"Welcome!!"
#define LABEL_TAPPLAY @"Tap Spin to Play"
#define LOCAL_NOTIFICATION_ALERT_BODY @"Big Daily Bonus Now! Play Big Time Profit Slots!"

#define DELAY_FOR_AUTO_SPIN 7
// SPIN Counter value..
#define SPIN_COUNTER_VALUE 70

#define kGameCenterLeaderboardID @"com.company.partyslot.leaderboard"

#define APP_URL @"https://itunes.apple.com/app/id235131?mt=8"

//#define APPIRATER_APPLE_APP_ID @"1095746168"

// -----------------------------------
// CUSTOM BUILT-IN DOWNLOAD POPUP
//
// if you want to have a custom popup asking for download of some other game,
// in addition to setting these up you'll also need to change the graphic of the download splash:
// replace images downloadview_small.png and downloadview_small@2x.png with your own
// -----------------------------------
#define SHOW_DOWNLOAD_POPUP NO
#define DOWNLOAD_NOW_URL @""

//flurry
#define FLURRY_APP_ID @""

// revmob
#define REVMOB_APP_ID @""

// startapp
#define STARTAPP_APP_ID @""
#define STARTAPP_DEV_ID @""

// if you comment out this line there will be no showing of ads on app resume
#define ADS_RESUME_FREQUENCY 5

// if you comment out this line there will be no showing of ads on spin during the game
#define ADS_SPIN_FREQUENCY 10

//#define SPIN_FREQUENCY_1 5
//#define SPIN_FREQUENCY_2 7
//#define SPIN_FREQUENCY_3 10

typedef enum {
    kNewThemeSharing,
    kNewLevelSharing,
    kHugeWinSharing,
    kEntersLobbySharing,
    kUnknownSharing
} ShareTypes;

//#define kSERVER_SIDE_URL @"http://sync1.okduk.com/sync_vegasslots.php"
//#define kURL_VERIFY_PURCHASE_RECEIPT @"http://sync1.okduk.com/transaction.php"
