//
//  QNSURLConnection.h
//  PartySlots
//
//  Created by Pavel Wasilenko on 27.04.17.
//
//

#import <Foundation/Foundation.h>

@interface QNSURLConnection : NSObject

+ (NSData *)sendSynchronousRequest:(NSURLRequest *)request
                 returningResponse:(__autoreleasing NSURLResponse **)responsePtr
                             error:(__autoreleasing NSError **)errorPtr;
    
@end
