//
//  SLBaseViewController.h
//  Slots
//
//  Created by Mup Userow on 24/06/2016.
//
//

#import <UIKit/UIKit.h>

#import "Reachability.h"
#import "GameCenterManager.h"
#import "SLAppDelegate.h"
#import "CommonUtilities.h"
#import <GameKit/GameKit.h>

#import "NSData+AES.h"
#import "NSData+Base64.h"
#import "NSString+Base64.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>

#include "ALSoundSource.h"

#import "UIImage+CustomNamingForDevice.h"

#import "Configure.h"

@interface SLBaseViewController : UIViewController<NSURLConnectionDelegate, GameCenterManagerDelegate,  GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate>
{
    IBOutlet UIWebView *webView, *jackpotView;
    
    NSArray *tableData;

    IBOutlet UITableView *usersTable;
    
    NSMutableArray *users;
    //NSMutableDictionary *item;
    IBOutlet UIView *viewLogin, *viewRegister, *viewConnect, *viewSearching, *viewPlay, *viewLoading, *viewWinner, *bettingArea,  *displayUser, *alertView, *noCoinsView;
    IBOutlet UIButton *changebet100, *changebet500, *changebet1000;
    
    IBOutlet UIButton *soundswitch;

    NSTimer *countdown;
    NSUInteger remainingSeconds;

    IBOutlet UIView* viewNoInternet;
    
    
    IBOutlet UILabel *cost1000, *cost3200, *cost8000, *cost20000, *cost80000, *cost200000, *loadingtext;
    
    GameCenterManager *gameCenterManager;
    NSString* currentLeaderBoard;

    NSMutableDictionary *item;
    
    IBOutlet SLAppDelegate *appDelegate;
    
    IBOutlet UINavigationBar *bar;

    NSMutableData* responseData;
    NSURLConnection *connection;
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    id<ALSoundSource> spinSound;
}
- (void) showLeaderboard;

- (IBAction)tryConnect:(id)sender;
- (void) checkNetworkStatus:(NSNotification *)notice;
- (IBAction)sound:(id)sender;
- (void)presetSoundButtons;

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (readwrite, copy, nonatomic) NSArray *tableData;
@property (nonatomic, retain) GameCenterManager *gameCenterManager;
@property (nonatomic, assign) int64_t currentScore;
@property (nonatomic, retain) NSString* currentLeaderBoard;
@property (nonatomic, retain) UILabel *currentScoreLabel;
@property (nonatomic, retain) id<ALSoundSource> spinSound;


+ (void)showCoinsView:(id)sender inController:(UIViewController*)controller;
+(void)playSoundEffect:(NSString*)soundEfectId;
+(void)pauseBGMusic;
+(void)resumeBGMusic;
-(void)playSpinSoundEffect:(NSString*)soundEfectId;
-(void)stopSpinSoundEffect;
-(void)playMusic:(NSString*)musicId;
-(void)stopMusic;
@end

// sounds
#define kSoundSpinning @"spin_seam_1.mp3"
#define kSoundFinishedSpin @"stop.mp3"
#define kSoundWon @"won.mp3"
#define kSoundCoinDrop @"coindrop.caf"
#define kSoundChestOpen @"chestopen.caf"
#define kSoundNudge @"nudge.caf"

//TODO:: Mobeen Commented this.. Because we are going to use only one AudioFile for all backgrounds..

//#define kMusicAqua @"aqua.mp3"
//#define kMusicFruit @"fruit.mp3"
//#define kMusicPirates @"treasure_island.mp3"
//#define kMusicZombie @"zombie.mp3"

#define kMusicBackground @"bgmusic.mp3"
