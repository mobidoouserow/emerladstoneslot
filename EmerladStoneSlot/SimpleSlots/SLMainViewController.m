//
//  SLMainViewController.m
//  Slots
//
//  Created by Pavel Wasilenko on 21.05.17.
//  Copyright © 2017 MD.io. All rights reserved.
//

#import "SLMainViewController.h"

@interface SLMainViewController ()

@end

@implementation SLMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)buttonPlayClick:(id)sender {
    SLAppDelegate* del = (SLAppDelegate*)[UIApplication sharedApplication].delegate;
    
    del.window.rootViewController = del.viewController;
    [del.window makeKeyAndVisible];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [super dealloc];
}

@end
