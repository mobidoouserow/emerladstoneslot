//
//  ViewController.h
//  SimpleSlots
//
//  Created by Mup Userow on 16/07/2016.
//  Copyright (c) 2016 MD.io. All rights reserved.
//

#import "SLBaseViewController.h"


@interface ViewController : SLBaseViewController {
    IBOutlet UIImageView *showWin, *showLevel;
    IBOutlet UILabel *displayCoins, *lblDisplayName, *lblDisplayLevel, *lblDisplayNameNOT, *lblDisplayLevelNOT;
    IBOutlet UIButton *btnMoveLeft, *btnMoveRight, *game1, *game2, *game3, *game4, *btnDownloadNow, *btnDownloadCancel;
    
    AVAudioPlayer *nudgeSound, *finishedSpinSound, *wonSound;
    
	int64_t  currentScore;
	IBOutlet UILabel *optionLvl, *optionXP, *lblAlert1, *lblAlert2, *lblAlert3;
    
    IBOutlet UIView *viewSettings, *viewDownload;
    
    IBOutlet UIImageView *moveCoin1, *moveCoin2, *moveCoin3, *moveCoin4, *moveCoin5, *moveCoin6, *moveCoin7, *moveCoin8, *moveCoin9;
    
    IBOutlet UIScrollView *games;
        
    IBOutlet UISwitch *switchSound, *switchNotif, *switchPublic;
    
    IBOutlet UIButton *downloadNow, *downloadCancel;
    
    IBOutlet UIButton *video;
    
    NSError* lastError;
    bool isGameCenterAvailable;

    
    
    int moveRow1Counter, moveRow2Counter, moveRow3Counter, moveRow4Counter, moveRow5Counter;
    int moveRow1Amount, moveRow2Amount, moveRow3Amount, moveRow4Amount, moveRow5Amount;
    int row1Postion, row2Postion, row3Postion, row4Postion, row5Postion;
    int nudgeCounter, currentWinCoins, wonCounter, holdCounter, chestWin, coinsAdd, prizeShuffleCounter, winPostion, lblCounter;
    int linesToSpin, showWinItems, showWinItemsCounter, betAmount, totalWinAmount, moveSlides;
    
    NSTimer *row, *nudgeTimer, *wonTimer, *endNudgeTimer, *goldenTimer, *addWin, *showWinLines, *autoSpinTimer;
    NSString *play, *finished1, *finished2, *finished3, *finished4, *finished5, *won, *nudgeStatus, *cheatStatus, *cheatInfo, *nudgeLastTime, *endNudgeStatus, *holdStatus1, *holdStatus2, *holdStatus3, *holdStatus4, *holdStatus5, *holdStatus, *holdPressed1, *holdPressed2, *holdPressed3, *holdPressed4, *holdPressed5, *spinSafe, *chestWinStatus, *winFlashStatus, *addWinStatus, *advertStatus, *showWinStatus, *defaultMessages, *autoSpinStatus;
    NSMutableArray *showWinData;
    
    int moveCoin1By, moveCoin2By, moveCoin3By, moveCoin4By, moveCoin5By, moveCoin6By, moveCoin7By, moveCoin8By, moveCoin9By;

    NSTimer *bgDropCoinsTimer;
    
}

-(int)giveRandom:(int)by;
 
-(NSString *)randomCoinImg;

-(IBAction)downloadCancel:(id)sender;
-(IBAction)downloadNow:(id)sender;

- (IBAction)coinsView:(id)sender;
- (IBAction)moreGamesView:(id)sender;
- (IBAction)showProfile:(id)sender;

- (IBAction)Game1:(id)sender;
- (IBAction)Game2:(id)sender;
- (IBAction)Game3:(id)sender;
- (IBAction)Game4:(id)sender;

- (IBAction)moveLeft:(id)sender;
- (IBAction)moveRight:(id)sender;

- (IBAction)changePublic:(id)sender;
- (IBAction)changeNotif:(id)sender;
- (IBAction)changeSound:(id)sender;

-(void)manageServer:(NSString *)where;
- (NSString*)isLevelUnlocked:(int)level;

-(void)sortLevelBar;
-(NSString*)returnLevel:(int)exp;

// Settings popup
- (IBAction)hideSettings:(id)sender;
- (IBAction)showSettings:(id)sender;

// GameCenter
- (void) submitScore : (int) curScore;

-(void) authenticate;


@end
