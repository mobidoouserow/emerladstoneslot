//
//  main.m
//  SimpleSlots
//
//  Created by Mup Userow on 16/07/2016.
//  Copyright (c) 2016 MD.io. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SLAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SLAppDelegate class]));
    }
}
